# [Section] Lists
names = ["John", "Paul", "George", "Ringo"] #string list
programs = [ 'developer career', 'pi-shape', 'short courses'] #string list
durations = [260, 180, 20] #number list
truth_values = [ True, False, True, True, False ] #boolean list

sample_list = ["Apple", 3, False, 'Potato', 4, True]

# getting list size
print(len(programs))

# accessing values
# first element
print(programs[0])
# last element
print(programs[-1])
# negative index
print(names[-3])

print(durations)
print(programs[0:2])

students = ["Mary", "Matthew", "Tom", "Anna", "Thomas"]
grades = [100, 85, 88, 90, 75]

count = 0
while count < len(students): 
	print(f"The grade of {students[count]} is {grades[count]}")
	count += 1

# updating lists
print(programs[2])
programs[2] = "Short Courses"
print(f"new value: {programs[2]}")

# [Section] List Manipulation
# append()
programs.append('global')
print(programs)

# deleting of elements
durations.append(360)
print(durations)
del durations[-1]
print(durations)

# membership checks
print(20 in durations)
print(500 in durations)
print("short courses" in programs)

# sorting lists
print(names)
names.sort()
print(names)

# clear()
test_list = [1,2,3,4,5]
test_list.clear()
print(test_list)

# [Section] Dictionaries
person1 = {
	"name": "Brandon",
	"age": 28, 
	"occupation": "student",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Django"]
}
print(len(person1))

# accessing values
print(person1["name"])
# print(person1.name) - cannot be used on a manually created object

print(person1.keys())
print(person1.values())
print(person1.items())

# add key-value pairs
person1["nationality"] = "Filipino"
person1.update({"fave_food" : "sinigang"})
print(person1)

# delete key-value pairs
person1.pop("fave_food")
del person1["nationality"]
print(person1)

person2 = {
	"name": "John"
}
print(person2)
person2.clear()
print(person2)

# looping through dict
for key in person1: 
	print(f"The value of {key} is {person1[key]}")

person3 = {
	"name": "Monika",
	"age": 20, 
	"occupation": "poet",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Django"]
}

classroom = {
	"student1": person1, 
	"student2": person3
}
print(classroom)